import 'bulma/css/bulma.css';
import React, {useState} from 'react';

type CellValue = 'X' | 'O' | null

function calculateWinner(squares: ReadonlyArray<CellValue>): CellValue {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (const [a, b, c] of lines) {
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}

interface BoardProps {
  readonly squares: ReadonlyArray<CellValue>
  readonly xIsNext: boolean
}

function Board(props: BoardProps & Readonly<{ onClick: (i: number) => void }>) {
  const size = 300;
  return (
    <svg
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      width={size}
      height={size}
    >
      {props.squares.map((c, i) => {
        const length = size / 3;
        const x = i % 3 * length;
        const y = Math.floor(i / 3) * length;
        const cx = x + length / 2;
        const cy = y + length / 2;
        const onClick = c ? undefined : () => props.onClick(i);
        const rect = (<rect
          x={x + 2}
          y={y + 2}
          width={length - 4}
          height={length - 4}
          rx="6"
          ry="6"
          fill="rgba(255, 255, 255, 0)"
          stroke="#111111"
          onClick={onClick}
        />);
        let mark;
        switch (c) {
          case 'X':
            const a = length * 0.9;
            const b = length * 0.1;
            mark = (
              <g
                fill="#53B0FF"
                transform={`rotate(45 ${cx} ${cy})`}
              >
                <rect
                  x={cx - a / 2}
                  y={cy - b / 2}
                  rx={b / 2}
                  width={a}
                  height={b}
                />
                <rect
                  x={cx - b / 2}
                  y={cy - a / 2}
                  ry={b / 2}
                  width={b}
                  height={a}
                />
              </g>
            );
            break;
          case 'O':
            mark = (
              <circle
                cx={cx}
                cy={cy}
                r={length / 3.5}
                fill="none"
                stroke="#FF972D"
                strokeWidth={length / 10}
              />
            );
            break;
          default:
            break;
        }
        return (
          <React.Fragment key={i}>
            {mark}
            {rect}
          </React.Fragment>
        );
      })}
    </svg>
  );
}

function Game() {
  const [history, setHistory] = useState([{
    squares: Array(9).fill(null),
    xIsNext: true,
  }]);

  const board = history[history.length - 1];
  const winner = calculateWinner(board.squares);

  function handleClick(i: number): void {
    if (winner !== null) {
      return;
    }
    if (board.squares[i] !== null) {
      return;
    }
    const s = board.squares.slice();
    s[i] = board.xIsNext ? 'X' : 'O';
    setHistory(history.concat({
      squares: s,
      xIsNext: !board.xIsNext,
    }));
  }

  function jumpTo(i: number): void {
    setHistory(history.slice(0, i + 1));
  }

  let status;
  if (winner) {
    status = `winner: ${winner}`;
  } else {
    status = `next player: ${board.xIsNext ? 'X' : 'O'}`
  }

  const moves = history
    .slice(0, history.length - 1)
    .map((board, move) => {
      const desc = move === 0 ? 'Go to game start' : `Go to #${move}`;
      return (
        <li key={move}>
          <button onClick={() => jumpTo(move)}>{desc}</button>
        </li>
      );
    });

  return (
    <div className="container">
      <div className="content">
        <Board
          squares={board.squares}
          xIsNext={board.xIsNext}
          onClick={handleClick}
        />
      </div>
      <div className="content">
        <div>{status}</div>
      </div>
      <div className="content">
        <ol>{moves}</ol>
      </div>
    </div>
  );
}

function App() {
  return (
    <div>
      <section className="section">
        <div className="container">
          <div className="content">
            <h1>React Tutorial: implement the game of Tic tac toe</h1>
            <p>
              This is a <a href="https://ja.reactjs.org/tutorial/tutorial.html">tutorial of React</a>.
            </p>
            <p>
              <a href="https://github.com/likr-sandbox/convex-hull/blob/master/src/App.js">尾上さんのレポジトリ</a>も参考にしている。
            </p>
          </div>
        </div>
      </section>
      <section className="section">
        <Game/>
      </section>
      <div className="footer">
        <div className="container">
          <div className="content">
            <p>&copy; 2020 Daiju Nakayama</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
